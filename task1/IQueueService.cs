﻿using System.Threading.Tasks;

namespace AV.Test1.Services
{
    public interface IQueueAsync<T>
    {
        void PushAsync(T item);
        Task<T> PopAsync();
    }
}

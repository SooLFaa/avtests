﻿using System;
using System.Threading.Tasks;
using AV.Test1.Services;

namespace AV.Test1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Позволю себе намешать логику для простоты примера.
            IQueueAsync<Int32> queue = new QueueAsync<Int32>();
            queue.PushAsync(111);
            queue.PushAsync(1110000000);
            Int32 number = queue.PopAsync().Result;
            Console.WriteLine(number);
            Task.Run(() => queue.PushAsync(0x10101010));
            number = queue.PopAsync().Result;
            Console.WriteLine(number);
            queue.PushAsync(10);
            Console.ReadKey();
        }
    }
}

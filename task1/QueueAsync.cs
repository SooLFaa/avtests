﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AV.Test1.Services
{
    public class QueueAsync<T>: IQueueAsync<T>
    {
        private Queue<T> queue;

        public QueueAsync()
        {
            queue = new Queue<T>();
        }

        public async void PushAsync(T item)
        {
           await Task.Run(() => queue.Enqueue(item));
        }

        public async Task<T> PopAsync()
        {
            return await Task.Run(() => queue.Dequeue());
        }
    }
}

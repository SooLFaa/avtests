﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AV.Test2
{
    public class Program
    {
        public static void Main(string[] args)
        {
			// Захардкодим. Понимаю что это плохо, но для простоты примера. 
            List<Int32> numbers = new List<Int32>() { 10, 2, 30, 1, 12, 11, 9, 44, 55, 3, 7 };
            
            int x;

            if (!int.TryParse(args.FirstOrDefault(), out x))
            {
                // Хорошо бы исключение сгнерить в отдельном классе, но для простоты не будем :)
                Console.WriteLine("Параметр отсуствует или не является целым числом");
                Console.ReadKey();
                return;
            }

            Dictionary<Int32, Int32> results = new Dictionary<Int32, Int32>();
            calcAllNumberPairsEqualsX(numbers, results, x);

            foreach (KeyValuePair<Int32, Int32> pair in results)
            {
                Console.WriteLine(String.Concat(pair.Key, ":", pair.Value));
            }

            Console.ReadKey();
        }

        private static void calcAllNumberPairsEqualsX(List<Int32> numbers, Dictionary<Int32, Int32> results, int x)
        {
            Int32 currentItem = numbers.First();
            numbers.RemoveAt(0);

            if (numbers.Count == 0)
            {
                return;
            }

            foreach (Int32 number in numbers)
            {
                if (currentItem + number == x)
                {
                    results.Add(currentItem, number);
                }
            }

            calcAllNumberPairsEqualsX(numbers, results, x);
        }
    }
}
